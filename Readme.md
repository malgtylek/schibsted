Application should be run on some server like nginx or tomcat.

I used function constructors(ES5 class interpretation) with full encapsulation to make reusable components.

XML and JSON Parsers were implemented by myself.

Optimization:
done:
- defer scripts
- dry
- minification (uglify-js - index.min.js)

todo: 
- gzip (only with proper config dependent on server)
- css minification
- obfuscation
- critical path optimization
- cache control

