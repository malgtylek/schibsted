(function () {
    function Util() {
    }

    /**
     *
     * @param {string} tag
     * @param {Object} object
     * @returns {HTMLElement}
     */
    Util.createElement = function (tag, object) {
        object = object || {};

        var domElem = document.createElement(tag);

        Object.keys(object).forEach(function (key) {
            domElem[key] = object[key];

            if (key === 'data') {
                Util.dataAttributeHandle(domElem, object.data);
            }
        });

        return domElem;
    };

    /**
     *
     * @param {HTMLElement} omElem
     * @param {Object} dataset
     */
    Util.dataAttributeHandle = function (domElem, dataset) {
        Object.keys(dataset).forEach(function (dataAttribute) {
            domElem.dataset[dataAttribute] = dataset[dataAttribute];
        })
    };

    /**
     *
     * @param {Array<string>} options
     * @param {string} defaultValue
     * @constructor
     */
    function DropDown(options, defaultValue) {
        var container = Util.createElement('div', {className: 'response-type'}),
            dropdown = Util.createElement('div', {className: 'dropdown primary-dropdown'}),
            caret = Util.createElement('span', {className: 'caret caret-arrow'}),
            button = Util.createElement('button', {
                id: 'reponseTypeButton',
                innerText: defaultValue,
                className: 'btn btn-primary dropdown-toggle dropdown-btn',
                type: 'button',
                data: {toggle: 'dropdown'}
            }),
            clickHandler,
            dropdownMenu = Util.createElement('div', {id: 'menu', className: 'dropdown-menu dropdown-menu-right'});

        dropdownMenu.addEventListener('click', function (event) {
            if (clickHandler) {
                clickHandler(event);
            }
        });

        DropDown.createOptions(options, dropdownMenu);

        button.appendChild(caret);
        dropdown.appendChild(button);
        dropdown.appendChild(dropdownMenu);
        container.appendChild(dropdown);

        /**
         * @param {string} string
         */
        this.setValue = function (string) {
            button.childNodes[0].nodeValue = string;
        };

        this.onclick = function (callback) {
            clickHandler = callback;
        };

        this.appendTo = function (target) {
            target.appendChild(container);
        };
    }

    /**
     *
     * @param {Array<string>} options
     * @param {HTMLElement} menu
     */
    DropDown.createOptions = function (options, menu) {
        options.forEach(function (option) {
            var hrefElem = Util.createElement('a', {href: '#', innerText: option, id: option}),
                li = Util.createElement('li');

            li.appendChild(hrefElem);
            menu.appendChild(li);
        });
    };

    /**
     *
     * @param {string} title
     * @param {HTMLElement} targetElement
     * @constructor
     */
    function AjaxComponent(title, targetElement) {
        var self = this,
            body = Util.createElement('div', {className: 'root grid'}),
            header = Util.createElement('h2', {innerText: title, className: 'header'}),
            dropdown = new DropDown(['json', 'xml'], 'JSON'),
            request = new RequestComponent('GET', ''),
            response = new ResponseComponent();

        init(this);

        targetElement.appendChild(body);

        /**
         *
         * @returns {RequestComponent}
         */
        this.getRequest = function () {
            return request;
        };

        /**
         *
         * @returns {ResponseComponent}
         */
        this.getResponse = function () {
            return response;
        };

        /**
         * @private
         * @param {string} requestName
         * @param {string} format
         * @param {AjaxComponent} ajaxComponent
         * @returns {Object}
         */
        function getData(requestName, format, ajaxComponent) {
            var data = {},
                request = requestName + '.' + format,
                xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function (event) {
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    data = xhttp.response;
                    self.getRequest().update(xhttp.responseURL);
                    self.getResponse().update(xhttp, format);
                }
            };
            xhttp.open("GET", request, true);
            xhttp.send();

            return data;
        }

        function init(self) {
            var requestName = 'advertisers',
                format = 'json',
                data;

            body.appendChild(header);
            dropdown.appendTo(body);
            request.appendTo(body);
            response.appendTo(body);

            dropdown.onclick(function (event) {
                if (event.srcElement.id !== format) {
                    format = event.srcElement.id;
                    data = getData(requestName, format, self);
                    dropdown.setValue(format.toUpperCase());
                }
            });

            data = getData(requestName, format, self);
        }
    }

    /**
     *
     * @param {string} method
     * @param {string} path
     * @constructor
     */
    function RequestComponent(method, path) {
        var request = Util.createElement('div', {className: 'request coded-field small-cf'}),
            requestMethod = Util.createElement('span', {innerText: method, className: 'bold small-space'}),
            requestPath = Util.createElement('span', {innerText: path, className: 'small-font'});

        request.appendChild(requestMethod);
        request.appendChild(requestPath);

        this.appendTo = function (target) {
            target.appendChild(request);
        };

        /**
         *
         * @param {string} responseURL
         */
        this.update = function (responseURL) {
            var location = window.location.origin;
            requestPath.innerText = responseURL.replace(location, '');
        }
    }

    /**
     *
     * @constructor
     */
    function ResponseComponent() {
        var response = Util.createElement('div', {className: 'response coded-field big-cf'}),
            headers = Util.createElement('div', {id: 'responseHeaders', className: 'bottom-space'}),
            responseData = Util.createElement('div', {id: 'responseData', className: 'bottom-space'});

        response.appendChild(headers);
        response.appendChild(responseData);

        this.appendTo = function (target) {
            target.appendChild(response);
        };

        /**
         * @private
         * @param {Array<string>} headers
         * @param {HTMLElement} responseDataComponent
         */
        function setHeaders(headers, responseDataComponent) {
            headers.forEach(function (header) {
                if (header) {
                    var headerArray = header.split(':'),
                        headerProp = Util.createElement('span', {innerText: headerArray[0] + ':', className: 'bold'}),
                        headerValue = Util.createElement('span', {innerText: headerArray[1], className: 'blue'}),
                        headerComponent = Util.createElement('div', {className: 'bottom-space'});

                    headerComponent.appendChild(headerProp);
                    headerComponent.appendChild(headerValue);
                    responseDataComponent.appendChild(headerComponent);
                }
            });
        }

        /**
         *
         * @param {Object} response
         * @param {string} format
         */
        this.update = function (response, format) {
            format = format || 'json';
            var status = 'HTTP ' + response.status + ' ' + response.statusText,
                statusComponent = Util.createElement('div', {className: 'bold bottom-space'}),
                headersComponent = Util.createElement('div', {});

            headers.innerHTML = '';
            responseData.innerHTML = '';

            statusComponent.innerText = status;
            setHeaders(response.getAllResponseHeaders().split(/\r?\n/), headersComponent);
            headers.appendChild(statusComponent);
            headers.appendChild(headersComponent);

            if (format.toLowerCase() === 'json') {
                responseData.appendChild(Parser.parseJSON(response.responseText));
            } else {
                responseData.appendChild(Parser.parseXML(response.response));
            }
        }
    }

    var oneTab = '&nbsp;&nbsp;';

    function Parser() {
    }

    /**
     *
     * @param {string} response
     * @returns {DocumentFragment}
     */
    Parser.parseJSON = function (response) {
        var nodeDepth = 0,
            jsonStructured = JSON.parse(response),
            fragment = document.createDocumentFragment();

        function recursive(node) {
            var wrappedElem,
                startingNodeCharacter,
                endingNodeCharacter,
                nodeKeys,
                lastIndex;

            if (!Parser.isSimpleType(node)) {
                if (Array.isArray(node)) {
                    startingNodeCharacter = '[';
                    endingNodeCharacter = ']';
                } else {
                    startingNodeCharacter = '{';
                    endingNodeCharacter = '}';
                }
                startingNodeCharacter = oneTab.repeat(nodeDepth) + startingNodeCharacter;
                endingNodeCharacter = oneTab.repeat(nodeDepth) + endingNodeCharacter;

                ++nodeDepth;
                fragment.appendChild(Util.createElement('div', {innerHTML: startingNodeCharacter}));
            }

            nodeKeys = Object.keys(node);
            lastIndex = nodeKeys.length - 1;
            nodeKeys.forEach(function (value, index) {
                if (Parser.isSimpleType(node[value])) {
                    wrappedElem = Parser.wrapSimpleJSONNode(value, node[value], nodeDepth, index === lastIndex);
                    fragment.appendChild(wrappedElem);
                } else {
                    ++nodeDepth;
                    recursive(node[value]);
                }
            });

            if (endingNodeCharacter) {
                --nodeDepth;
                fragment.appendChild(Util.createElement('div', {innerHTML: endingNodeCharacter}));
            }
        }

        recursive(jsonStructured);
        return fragment;
    };

    Parser.parseXML = function (responseText) {
        var parser = new DOMParser(),
            responseXml = parser.parseFromString(responseText, "text/xml"),
            nodeDepth = 0,
            fragment = document.createDocumentFragment();

        function recursive(root) {
            if (root && root.children.length > 0) {
                fragment.appendChild(Parser.createXMLParent(nodeDepth, root.nodeName, false, false));

                ++nodeDepth;

                for (var i = 0; i < root.children.length; i++) {
                    recursive(root.children[i]);
                }

                fragment.appendChild(Parser.createXMLParent(nodeDepth, root.nodeName, true, false));
            } else {
                fragment.appendChild(Parser.createXMLChild(nodeDepth, root.nodeName, root.innerHTML));
            }
        }

        recursive(responseXml.firstChild);
        return fragment;
    };

    /**
     *
     * @param {string} nodeName
     * @param {string} nodeValue
     * @param {number} tabAmount
     * @returns {HTMLElement}
     */
    Parser.wrapSimpleJSONNode = function (nodeName, nodeValue, tabAmount, isLastElement) {
        var tabs = oneTab.repeat(tabAmount),
            nodeNameElement = Util.createElement('span', {innerHTML: tabs + "\"" + nodeName + "\"", className: 'red'}),
            separatorElement = Util.createElement('span', {innerText: ': ', className: 'gray'}),
            nodeValueElement = Util.createElement('span', {
                innerHTML: Parser.getWrappedValue(nodeValue),
                className: Parser.getColourClass(nodeValue)
            }),
            commaElement = Util.createElement('span', {innerText: ',', className: 'gray'}),
            nodeElement = Util.createElement('div', {className: 'bottom-space break'});

        nodeElement.appendChild(nodeNameElement);
        nodeElement.appendChild(separatorElement);
        nodeElement.appendChild(nodeValueElement);

        if (!isLastElement) {
            nodeElement.appendChild(commaElement);
        }

        return nodeElement;
    };

    /**
     *
     * @param value
     * @returns {string}
     */
    Parser.getColourClass = function (value) {
        if (typeof value === "string") {
            return 'red';
        }

        if (typeof value === "number" || typeof value === "boolean") {
            return 'blue';
        }

        return 'gray';
    };

    /**
     *
     * @param value
     * @returns {*}
     */
    Parser.getWrappedValue = function (value) {
        if (typeof value === "string") {
            return "\"" + value + "\"";
        }
        return value;
    };

    Parser.isSimpleType = function (value) {
        return ['boolean', 'number', 'string'].indexOf(typeof value) !== -1;
    };

    Parser.createXMLParent = function (nodeDepth, nodeName, isClosing, inline) {
        var open,
            name = Util.createElement('span', {innerHTML: nodeName, className: 'red'}),
            close, parentElement;

        if (isClosing) {
            close = Util.createElement('span', {innerHTML: '/>', className: 'gray'});
            open = Util.createElement('span', {innerHTML: '<', className: 'gray'});
        } else {
            open = Util.createElement('span', {innerHTML: oneTab.repeat(nodeDepth) + '<', className: 'gray'});
            close = Util.createElement('span', {innerHTML: '>', className: 'gray'});
        }

        if (inline) {
            parentElement = Util.createElement('span', {className: 'bottom-space'});
        } else {
            parentElement = Util.createElement('div', {className: 'bottom-space'});
        }

        parentElement.appendChild(open);
        parentElement.appendChild(name);
        parentElement.appendChild(close);
        return parentElement;
    };

    Parser.createXMLChild = function (nodeDepth, name, value) {
        var nameLeft = Parser.createXMLParent(nodeDepth, name, false, true),
            valueElement = Util.createElement('span', {innerHTML: value, className: 'blue'}),
            nameRight = Parser.createXMLParent(nodeDepth, name, true, true),
            childElement = Util.createElement('div', {className: 'bottom-space break'});

        childElement.appendChild(nameLeft);
        childElement.appendChild(valueElement);
        childElement.appendChild(nameRight);
        return childElement;
    };


    function init() {
        new AjaxComponent('Advertiser List', document.body);
    }

    init();
}());